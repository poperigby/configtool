import os

from setuptools import setup

modules = []
if os.environ.get("ARCH") == "openmw":
    modules.append("openmw.pmodule")
elif os.environ.get("ARCH") in ("oblivion", "fallout-nv", "skyrim", "fallout-4"):
    modules.append("bethesda.pmodule")

setup(
    name="configtool",
    version="0.6.4",
    description="Portmod module for updating game config files",
    author="Benjamin Winger",
    author_email="bmw@disroot.org",
    url="https://gitlab.com/portmod/configtool",
    packages=["configtool"],
    install_requires=["roundtripini>=0.2"],
    data_files=[("modules", modules)],
)
