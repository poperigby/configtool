# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""Module for updating and sorting openmw config sections"""

import csv
import fnmatch
import glob
import json
import os
import re
from collections import defaultdict
from configparser import ConfigParser
from functools import lru_cache
from logging import warning
from typing import Any, Dict, List, Mapping, NamedTuple, Optional, Set, Tuple, cast

from portmod.globals import env
from portmod.loader import (
    load_all_installed,
    load_all_installed_map,
    load_installed_pkg,
)
from portmod.tsort import CycleException, tsort
from portmodlib.atom import Atom, atom_sat
from portmodlib.execute import execute
from portmodlib.usestr import use_reduce

from .configfile import remove_config
from .files import get_directories, get_files

ROOT = os.environ["ROOT"]


class File(NamedTuple):
    name: str
    overrides: List[str]


class ConfigData(NamedTuple):
    id: Atom
    """
    Package name and category identifying this entry

    Must parse as an atom
    """
    path: str
    """Absolute path to the directory this VFS entry refers to"""
    groundcover: List[File] = []
    plugins: List[File] = []
    archives: List[File] = []
    tier: str = "a"
    overrides: List[Atom] = []
    flags: List[str] = []
    fallback: Optional[Dict[str, Dict[str, str]]] = None
    settings: Optional[Dict[str, Dict[str, str]]] = None


def map_data(jsondata: Dict):
    result = {}
    for key, value in jsondata.items():
        if key == "id":
            result["id"] = Atom(value)
        elif key in ("groundcover", "plugins", "archives"):
            result[key] = [File(**file) for file in value]
        elif key == "overrides":
            result[key] = [Atom(atom) for atom in value]
        else:
            result[key] = value
    return result


@lru_cache()
def get_config_files() -> List[ConfigData]:
    results = []
    configpath = os.environ.get("CONFIGPATH")
    if configpath:
        for path in glob.iglob(
            os.path.join(ROOT, configpath, "configtool", "**/*.json"), recursive=True
        ):
            with open(path) as file:
                results.append(ConfigData(**map_data(json.load(file))))

    return results


def get_config_info(atom: Atom) -> Optional[ConfigData]:
    for data in get_config_files():
        if atom_sat(data.id, atom):
            return data
    return None


def get_vfs_dirs():
    installed_dict = load_all_installed_map()
    installed = [mod for group in installed_dict.values() for mod in group]

    graph: Dict[Tuple[str, str, bool], Set[Tuple[str, str, bool]]] = {}
    priorities = {}

    user_config_path = os.path.join(
        os.environ["PORTMOD_CONFIG_DIR"], "config", "install.csv"
    )
    userconfig: Dict[str, Set[str]] = read_userconfig(
        user_config_path, bool(os.environ.get("CASE_INSENSITIVE_FILES", False))
    )

    # Determine all Directories that are enabled
    for mod in installed:
        for install in get_directories(mod):
            if install.VFS or install.VFS is None:
                default = os.path.normpath(install.PATCHDIR) == "."
                path = mod.get_dir_path(install)
                graph[(mod.ATOM.CP, path, default)] = set()
                priorities[(mod.CP, path, default)] = mod.TIER

    for file in get_config_files():
        graph[(file.id, file.path, False)] = set()
        priorities[(file.id, file.path, False)] = file.tier

    # Add edges in the graph for each data override
    def add_directory(ipath: str, pkg: Atom, parents: Set[Atom]):
        for parent in parents:
            if parent.USE and not usedep_matches_installed(parent):
                continue

            for (atom, path, default) in graph:
                if atom_sat(Atom(atom), parent) and default:
                    if Atom(atom).BLOCK:
                        # Blockers have reversed edges
                        graph[(pkg.CP, ipath, idefault)].add((atom, path, default))
                    else:
                        graph[(atom, path, default)].add((pkg.CP, ipath, idefault))

    for mod in installed:
        for install in get_directories(mod):
            if install.VFS is False:
                continue
            idefault = os.path.normpath(install.PATCHDIR) == "."
            ipath = mod.get_dir_path(install)
            parents = set(
                use_reduce(
                    mod.DATA_OVERRIDES + " " + install.DATA_OVERRIDES,
                    mod.INSTALLED_USE,
                    flat=True,
                    token_class=Atom,
                )
            ) | {
                Atom(override)
                for name in userconfig
                for override in userconfig[name]
                if atom_sat(mod.ATOM, Atom(name))
            }
            add_directory(ipath, mod.ATOM, parents)

    for file in get_config_files():
        parents = set(Atom(token) for token in file.overrides)
        add_directory(file.path, file.id, parents)

    try:
        sorted_mods = tsort(graph, priorities)
    except CycleException as error:
        raise CycleException(
            "Encountered cycle when sorting vfs!", error.cycle
        ) from error

    return [path for _, path, _ in sorted_mods]


def get_masters(plugin: str) -> List[str]:
    _, ext = os.path.splitext(plugin.lower())
    if ext in (".omwaddon", ".esp", ".esm", ".esl", ".omwgame"):
        return cast(
            str, execute(f'delta_plugin deps "{plugin}"', pipe_output=True)
        ).splitlines()
    return []


class Config:
    def __init__(
        self,
        path: str,
        typ: str,
        aux: Optional[str] = None,
        section_pattern: Optional[str] = None,
        entry_pattern: Optional[str] = None,
        section: Optional[str] = None,
        key: Optional[str] = None,
        value: Optional[str] = None,
        spaces_to_underscores: bool = False,
        ini: bool = False,
        file=None,
    ):
        self.path = os.path.expanduser(path)
        self.typ = typ
        self.aux = aux
        self.section_pattern = section_pattern
        self.entry_pattern = entry_pattern
        self.section = section
        self.key = key
        self.value = value
        self.spaces_to_underscores = spaces_to_underscores
        self.ini = ini
        self.file = file


def fnmatch_dict(dictionary: Mapping[str, Any], pattern: str) -> Optional[str]:
    """
    Returns a key that matches the given fnmatch-style pattern.
    Matches in a case insensitive value
    """
    for key in dictionary:
        if fnmatch.fnmatch(key.lower(), pattern.lower()):
            return key
    return None


def usedep_matches_installed(atom: Atom) -> bool:
    mod = load_installed_pkg(atom.strip_use())
    if mod:
        flags = atom.USE
    else:
        pkg = get_config_info(atom.CPN)
        if pkg:
            flags = pkg.flags
        else:
            return False  # If override isn't installed, it won't be in the graph

    for flag in flags:
        if flag.startswith("-") and flag.lstrip("-") in mod.INSTALLED_USE:
            return False  # Required flag is not set
        elif not flag.startswith("-") and flag not in mod.INSTALLED_USE:
            return False  # Required flag is not set

    return True


def config_entry_pattern(config, **kwargs):
    return config_pattern(config, config.entry_pattern, **kwargs)


def config_section_pattern(config, **kwargs):
    return config_pattern(config, config.section, **kwargs)


def config_key_pattern(config, **kwargs):
    return config_pattern(config, config.key, **kwargs)


def config_value_pattern(config, **kwargs):
    return config_pattern(config, config.value, **kwargs)


def config_pattern(config, pattern, **kwargs):
    if config.spaces_to_underscores:

        def wrapper(s: str):
            return s.replace(" ", "_")

    else:

        def wrapper(s: str):
            return s

    if "section" not in kwargs:
        kwargs["section"] = config.section
    if "key" not in kwargs:
        kwargs["key"] = config.key
    if "value" not in kwargs:
        kwargs["value"] = config.value

    pattern_ = pattern
    while any("{" + key + "}" in pattern_ for key in kwargs):
        for key in kwargs:
            # Only turn spaces into underscores for sections and keys
            if key in {"key", "section"} or (
                config.aux and key in {config.aux + "_SECTION", config.aux + "_KEY"}
            ):
                pattern_ = pattern_.replace(
                    "{" + key + "}", wrapper(str(kwargs[key])) or ""
                )
            else:
                pattern_ = pattern_.replace("{" + key + "}", str(kwargs[key]) or "")
    return re.sub("{.*?}", "*", pattern_)


def commit_changes(config, new):
    if config.ini:
        inisection = config_section_pattern(config)
        newentries = {}
        for index, entry in enumerate(new):
            inikey = config_key_pattern(
                config, **{config.aux or config.typ: entry, "i": index}
            )
            inivalue = config_value_pattern(
                config, **{config.aux or config.typ: entry, "i": index}
            )
            newentries[inikey] = inivalue

        del config.file[inisection]
        for key, value in newentries.items():
            config.file[inisection, key] = value
    else:
        remove_config(config.file, f"{config.section}=*")
        for entry in new:
            config.file.append(config_entry_pattern(config, **{config.aux: entry}))


def read_userconfig(path: str, case_insensitive: bool) -> Dict[str, Set[str]]:
    userconfig = defaultdict(set)

    if os.path.exists(path):
        # Read user config
        with open(path, newline="") as csvfile:
            csvreader = csv.reader(csvfile, skipinitialspace=True)
            for row in csvreader:
                assert len(row) > 1
                atom = row[0].strip()
                if case_insensitive:
                    atom = atom.lower()

                userconfig[atom] |= set(map(lambda x: x.strip(), row[1:]))

    return userconfig


def sort_config(config: Config):
    """Regenerates managed sections of config files"""
    env.set_prefix(os.environ["PORTMOD_PREFIX_NAME"])

    def section_pattern(config, **kwargs):
        if "section" not in kwargs:
            kwargs["section"] = config.section
        if "key" not in kwargs:
            kwargs["key"] = config.key
        if "value" not in kwargs:
            kwargs["value"] = config.value

        pattern = config.section_pattern
        while any("{" + key + "}" in pattern for key in kwargs):
            for key in kwargs:
                pattern = pattern.replace("{" + key + "}", str(kwargs[key]) or "")
        return re.sub("{.*?}", "*", pattern)

    installed = load_all_installed()
    print(f"Sorting {config.path} {config.aux} entries...")

    if config.typ == "INSTALL":
        # Sort 'data' entries in config
        new_dirs = ['"' + path + '"' for path in get_vfs_dirs()]

        commit_changes(config, new_dirs)
    elif config.typ == "FILE":
        # Sort 'content' entries in config
        # Create graph of content files that are installed, with masters of a file
        # being the parent of the node in the graph
        # Any other content files found in the config should be warned about and
        # removed.
        graph: Dict[str, Set[str]] = {}
        priorities: Dict[str, str] = {}

        case_insensitive = bool(os.environ.get("CASE_INSENSITIVE_FILES", False))
        real_names = {}

        assert config.aux
        # Keys refer to master atoms (overridden).
        # values are a set of overriding mod atomso
        user_config_path = os.path.join(
            os.environ["PORTMOD_CONFIG_DIR"], "config", config.aux.lower() + ".csv"
        )
        # Keys refer to masters (overridden).
        # values are a set of overriding files
        userconfig: Dict[str, Set[str]] = read_userconfig(
            user_config_path, case_insensitive
        )

        # Determine all Files that are enabled
        for pkg in load_all_installed():
            for _, file in get_files(pkg, config.aux):
                name = file.NAME
                if case_insensitive:
                    name = file.NAME.lower()
                graph[name] = set()
                priorities[name] = pkg.TIER
                real_names[name] = file.NAME

        for config_file in get_config_files():
            for file in getattr(config_file, config.aux.lower(), []):
                name = file.name
                if case_insensitive:
                    name = name.lower()
                graph[name] = set()
                priorities[name] = config_file.tier
                real_names[name] = file.name

        # Validate entries in userconfig
        for entry in userconfig.keys() | {
            item for group in userconfig.values() for item in group
        }:
            if entry not in graph:
                warning(f"File {entry} in {user_config_path} could not be found!")

        for pkg in load_all_installed():
            for install, file in get_files(pkg, config.aux):
                name = file.NAME
                if case_insensitive:
                    name = file.NAME.lower()

                path = pkg.get_file_path(install, file)
                # We need a path to determine masters
                masters = set(get_masters(path))
                if isinstance(file.OVERRIDES, str):
                    masters |= set(use_reduce(file.OVERRIDES, pkg.INSTALLED_USE))
                else:
                    masters |= set(file.OVERRIDES)

                if file.NAME in userconfig:
                    masters |= set(userconfig[name])

                # Add edge from master to child
                for master in masters:
                    if case_insensitive and master.lower() in graph:
                        graph[master.lower()].add(name)
                    elif not case_insensitive and master in graph:
                        graph[master].add(name)

        for config_file in get_config_files():
            for file in getattr(config_file, config.aux.lower(), []):
                name = file.name
                if case_insensitive:
                    name = name.lower()

                path = os.path.join(config_file.path, file.name)
                # We need a path to determine masters
                masters = set(get_masters(path))
                masters |= set(file.overrides)

                if file.name in userconfig:
                    masters |= set(userconfig[file.name])

                # Add edge from master to child
                for master in masters:
                    if case_insensitive and master.lower() in graph:
                        graph[master.lower()].add(name)
                    elif not case_insensitive and master in graph:
                        graph[master].add(name)

        try:
            new_files = tsort(graph, priorities)
        except CycleException as e:
            raise CycleException(
                f"Encountered cycle when sorting {config}!", e.cycle
            ) from e

        commit_changes(config, [real_names[name] for name in new_files])
    elif config.typ == "FIELD":
        graph: Dict[Atom, Set[Atom]] = {}
        priorities: Dict[Atom, str] = {}
        field_data: Dict[Atom, Dict] = {}

        assert config.aux

        for mod in installed:
            dictionary = mod.get_installed_env().get(config.aux)
            if dictionary:
                graph[mod.CP] = set()
                priorities[mod.CP] = mod.TIER
                field_data[mod.CP] = dictionary

        for config_file in get_config_files():
            dictionary = getattr(config_file, config.aux.lower(), None)
            if dictionary:
                graph[config_file.id] = set()
                priorities[config_file.id] = config_file.tier
                field_data[config_file.id] = dictionary

        for mod in installed:
            for parent in use_reduce(
                mod.DATA_OVERRIDES, mod.INSTALLED_USE, flat=True, token_class=Atom
            ):
                if not usedep_matches_installed(parent):
                    continue

                # pylint: disable=dict-iter-missing-items
                for atom in graph:
                    if atom_sat(Atom(atom), parent):
                        if Atom(atom).BLOCK:
                            # Blockers are reversed
                            graph[mod.CP].add(atom)
                        else:
                            graph[atom].add(mod.CP)

        for config_file in get_config_files():
            for parent in config_file.overrides:
                if not usedep_matches_installed(parent):
                    continue

                # pylint: disable=dict-iter-missing-items
                for atom in graph:
                    if atom_sat(atom, parent):
                        if atom.BLOCK:
                            # Blockers are reversed
                            graph[mod.CP].add(atom)
                        else:
                            graph[atom].add(mod.CP)

        sections = defaultdict(dict)
        for atom in tsort(graph, priorities):
            for section, sectiondata in field_data[atom].items():
                sections[section].update(sectiondata)

        user_config_path = os.path.join(
            os.environ["PORTMOD_CONFIG_DIR"], "config", config.aux.lower() + ".cfg"
        )
        user_config = ConfigParser()
        user_config.read(user_config_path)
        for section in user_config.sections():
            for (key, value) in user_config.items(section):
                sections[section][key] = value

        if not config.ini:
            pattern = re.sub("{.*}", "*", config.section)
            remove_config(config.file, pattern)
        for section in sections:
            if config.ini:
                inisection = config_section_pattern(
                    config, **{config.aux + "_SECTION": section}
                )
                for index, key in enumerate(sections[section]):
                    inikey = config_key_pattern(
                        config, **{config.aux + "_KEY": key, "i": index}
                    )
                    inivalue = config_value_pattern(
                        config,
                        **{
                            config.aux + "_VALUE": str(sections[section][key]),
                            "i": index,
                        },
                    )
                    config.file[inisection, inikey] = inivalue
            else:
                for key in sections[section]:
                    config.file.append(
                        config_entry_pattern(
                            config,
                            **{
                                config.aux + "_SECTION": section,
                                config.aux + "_KEY": key,
                                config.aux + "_VALUE": str(sections[section][key]),
                            },
                        )
                    )
