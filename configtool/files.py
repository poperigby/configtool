# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from typing import Generator, Tuple

from portmod.pybuild import Pybuild
from portmodlib.pybuild import File, InstallDir
from portmodlib.usestr import check_required_use


def get_directories(package: Pybuild) -> Generator[InstallDir, None, None]:
    """
    Returns all enabled InstallDir objects in INSTALL_DIRS
    """
    if package._PYBUILD_VER == 1:
        for install_dir in package.INSTALL_DIRS:
            if check_required_use(
                install_dir.REQUIRED_USE, package.get_use(), package.valid_use
            ):
                yield install_dir


def get_files(
    package: Pybuild, typ: str
) -> Generator[Tuple[InstallDir, File], None, None]:
    """
    Returns all enabled files and their directories
    """
    if package._PYBUILD_VER == 1:
        for install_dir in get_directories(package):
            if hasattr(install_dir, typ):
                for file in getattr(install_dir, typ):
                    if check_required_use(
                        file.REQUIRED_USE, package.get_use(), package.valid_use
                    ):
                        yield install_dir, file
